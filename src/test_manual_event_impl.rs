#![allow(unused_variables)]
#![allow(dead_code)]

use std::time::Duration;

use crate as event_broadcast;

#[derive(Clone, Debug)]
enum Event {
	Gesture(u8),
	WeatherEvent(u16),
}

impl event_broadcast::WithTopic for Event {
	type Topic = Topic;
}

impl event_broadcast::ManagedEvent for Event {
	type Manager = TopicList;
}

#[derive(Clone, Copy, Debug)]
#[repr(usize)]
enum Topic {
	Gesture = 0,
	Weather = 1,
}

impl From<Event> for Topic {
	fn from(value: Event) -> Self {
		match value {
			Event::Gesture(_) => Topic::Gesture,
			Event::WeatherEvent(_) => Topic::Weather,
		}
	}
}

impl TryFrom<usize> for Topic {
	type Error = ();

	fn try_from(value: usize) -> Result<Self, Self::Error> {
		match value {
			0 => Ok(Topic::Gesture),
			1 => Ok(Topic::Weather),
			_ => Err(()),
		}
	}
}

impl From<Topic> for usize {
	fn from(value: Topic) -> Self {
		value as usize
	}
}

struct TopicList {
	event_senders: [event_broadcast::_TokioBroadcastSender<Event>; 2],
}

impl event_broadcast::ManageTopics for TopicList {
	type Event = Event;

	fn new(capacity: usize) -> Self {
		let gesture = event_broadcast::_create_broadcast(capacity);
		let weather = event_broadcast::_create_broadcast(capacity);

		TopicList { event_senders: [gesture.0, weather.0] }
	}

	fn get_sender(&self, topic: <Self::Event as event_broadcast::WithTopic>::Topic) -> &event_broadcast::_TokioBroadcastSender<Self::Event> {
		let index: usize = topic.into();
		&self.event_senders[index]
	}

	fn create_receiver(&self, topic: <Self::Event as event_broadcast::WithTopic>::Topic) -> event_broadcast::_TokioBroadcastReceiver<Self::Event> {
		let index: usize = topic.into();
		self.event_senders[index].subscribe()
	}
}

#[test]
fn send_and_try_receive() {
	let sender = event_broadcast::EventSender::new(10);
	let mut receiver = sender.new_receiver();
	receiver.subscribe(Topic::Gesture).unwrap();
	sender.broadcast(Event::Gesture(10)).unwrap();
	receiver.try_receive().unwrap();
}

#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn async_send_and_receive() {
	let sender = event_broadcast::EventSender::new(10);
	let mut receiver = sender.new_receiver();
	receiver.subscribe(Topic::Gesture).unwrap();
	receiver.subscribe(Topic::Weather).unwrap();

	let handle = tokio::spawn(async move {
		receiver.receive().await.unwrap();
		receiver.receive().await.unwrap();
	});

	sender.broadcast(Event::Gesture(10)).unwrap();
	sender.broadcast(Event::WeatherEvent(42)).unwrap();

	tokio::time::timeout(Duration::from_secs(1), handle).await.unwrap().unwrap();
}
