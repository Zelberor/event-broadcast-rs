#![allow(unused_variables)]
#![allow(dead_code)]

use std::time::Duration;

use crate as event_broadcast;
use event_broadcast::generate_topic;

#[generate_topic]
#[derive(Clone, Debug)]
pub enum Event {
	Gesture(u8),
	Weather(u16),
	Bongo { ree: usize },
}

#[test]
fn send_and_try_receive() {
	let sender = event_broadcast::EventSender::new(10);
	let mut receiver = sender.new_receiver();
	receiver.subscribe(EventTopic::Gesture).unwrap();
	sender.broadcast(Event::Gesture(10)).unwrap();
	receiver.try_receive().unwrap();
}

#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn async_send_and_receive() {
	let sender = event_broadcast::EventSender::new(10);
	let mut receiver = sender.new_receiver();
	receiver.subscribe(EventTopic::Gesture).unwrap();
	receiver.subscribe(EventTopic::Bongo).unwrap();

	let handle = tokio::spawn(async move {
		receiver.receive().await.unwrap();
		receiver.receive().await.unwrap();
	});

	sender.broadcast(Event::Gesture(10)).unwrap();
	sender.broadcast(Event::Bongo { ree: 42 }).unwrap();

	tokio::time::timeout(Duration::from_secs(1), handle).await.unwrap().unwrap();
}
