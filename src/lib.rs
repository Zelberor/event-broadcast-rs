#[cfg(test)]
mod test_macro_event;
#[cfg(test)]
mod test_manual_event_impl;

mod topic_traits;

use topic_traits::{ManageTopics, ManagedEvent};

pub use topic_traits::ManageTopics as _ManageTopics;
pub use topic_traits::ManagedEvent as _ManagedEvent;
pub use topic_traits::WithTopic;

use std::marker::PhantomData;
use std::pin::Pin;
use std::sync::Mutex;
use std::{
	collections::HashMap,
	future::Future,
	sync::{Arc, Weak},
	task::Poll,
};

pub use tokio::sync::broadcast::channel as _create_broadcast;
pub use tokio::sync::broadcast::error::SendError;
pub use tokio::sync::broadcast::Receiver as _TokioBroadcastReceiver;
pub use tokio::sync::broadcast::Sender as _TokioBroadcastSender;

pub use generate_topic_macro::generate_topic;

#[derive(Debug)]
pub enum TryReceiveError<EN> {
	Error(ReceiveError<EN>),
	Empty,
}

impl<EN> From<ReceiveError<EN>> for TryReceiveError<EN> {
	fn from(error: ReceiveError<EN>) -> Self {
		TryReceiveError::Error(error)
	}
}

#[derive(Debug)]
pub enum ReceiveError<EN> {
	Overflowed(EN, u64),
	Closed(EN),
}

#[derive(Clone)]
pub struct EventSender<T>
where
	T: ManagedEvent,
{
	topic_manager: Arc<<T as ManagedEvent>::Manager>,
}

impl<T> EventSender<T>
where
	T: ManagedEvent,
{
	pub fn new(capacity: usize) -> Self {
		EventSender {
			topic_manager: Arc::new(<T as ManagedEvent>::Manager::new(capacity)),
		}
	}

	pub fn new_receiver(&self) -> EventReceiver<T> {
		EventReceiver::new(Arc::downgrade(&self.topic_manager))
	}

	pub fn broadcast(&self, event: T) -> Result<(), SendError<T>> {
		self.topic_manager.get_sender(event.clone().into()).send(event)?;
		Ok(())
	}

	// todo: await subscriber
}

pub struct EventReceiver<T>
where
	T: ManagedEvent,
{
	topic_manager: Weak<<T as ManagedEvent>::Manager>,
	subscribed_topics: HashMap<usize, _TokioBroadcastReceiver<T>>,
}

impl<T> EventReceiver<T>
where
	T: ManagedEvent,
{
	fn new(topic_manager: Weak<<T as ManagedEvent>::Manager>) -> Self {
		EventReceiver {
			topic_manager,
			subscribed_topics: HashMap::new(),
		}
	}

	pub fn new_receiver(&self) -> Self {
		Self::new(self.topic_manager.clone())
	}

	pub fn receive<'a>(&'a mut self) -> EventReceiveFuture<T, impl Future<Output = Result<T, tokio::sync::broadcast::error::RecvError>> + 'a> {
		new_receiver_future(self)
	}

	pub fn try_receive(&mut self) -> Result<T, TryReceiveError<<T as WithTopic>::Topic>> {
		for (k, receiver) in self.subscribed_topics.iter_mut() {
			let topic = (*k).try_into().expect("Failed to convert receiver index into topic");
			match receiver.try_recv() {
				Ok(event) => return Ok(event),
				Err(err) => match err {
					tokio::sync::broadcast::error::TryRecvError::Empty => {}
					tokio::sync::broadcast::error::TryRecvError::Closed => return Err(ReceiveError::Closed(topic).into()),
					tokio::sync::broadcast::error::TryRecvError::Lagged(o) => return Err(ReceiveError::Overflowed(topic, o).into()),
				},
			}
		}
		Err(TryReceiveError::Empty)
	}

	pub fn subscribe(&mut self, topic: <T as WithTopic>::Topic) -> Result<(), ReceiveError<<T as WithTopic>::Topic>> {
		if !self.subscribed_topics.contains_key(&topic.into()) {
			let topic_manager = match self.topic_manager.upgrade() {
				Some(tm) => tm,
				None => return Err(ReceiveError::Closed(topic)),
			};
			self.subscribed_topics.insert(topic.into(), topic_manager.create_receiver(topic));
		}
		Ok(())
	}

	pub fn unsubscribe(&mut self, topic: <T as WithTopic>::Topic) {
		self.subscribed_topics.remove(&topic.into());
	}
}

pub struct EventReceiveFuture<T, F> {
	// Todo: somehow get rid of mutex
	futures: HashMap<usize, Mutex<Pin<Box<F>>>>,
	datatype: PhantomData<T>,
}

fn new_receiver_future<'a, T>(receiver: &'a mut EventReceiver<T>) -> EventReceiveFuture<T, impl Future<Output = Result<T, tokio::sync::broadcast::error::RecvError>> + 'a>
where
	T: ManagedEvent,
{
	let mut hashmap = HashMap::new();
	for (k, receiver) in receiver.subscribed_topics.iter_mut() {
		let future = Mutex::new(Box::pin(receiver.recv()));
		hashmap.insert(*k, future);
	}
	EventReceiveFuture {
		futures: hashmap,
		datatype: PhantomData::default(),
	}
}

impl<T, F> Future for EventReceiveFuture<T, F>
where
	T: ManagedEvent,
	F: Future<Output = Result<T, tokio::sync::broadcast::error::RecvError>>,
{
	type Output = Result<T, ReceiveError<<T as WithTopic>::Topic>>;

	fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> std::task::Poll<Self::Output> {
		for (k, future) in self.futures.iter() {
			let topic = (*k).try_into().expect("Failed to convert receiver index into topic");
			match future.lock().expect("Failed locking mutex").as_mut().poll(cx) {
				Poll::Ready(result) => {
					return Poll::Ready(match result {
						Ok(event) => Ok(event),
						Err(err) => Err(match err {
							tokio::sync::broadcast::error::RecvError::Closed => ReceiveError::Closed(topic),
							tokio::sync::broadcast::error::RecvError::Lagged(o) => ReceiveError::Overflowed(topic, o),
						}),
					})
				}
				Poll::Pending => {}
			}
		}
		Poll::Pending
	}
}
