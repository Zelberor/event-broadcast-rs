use tokio::sync::broadcast::Receiver as TokioBroadcastReceiver;
use tokio::sync::broadcast::Sender as TokioBroadcastSender;

pub trait WithTopic
where
	Self: Sized,
{
	type Topic: From<Self> + TryFrom<usize, Error = ()> + Into<usize> + Clone + Copy;
}

pub trait ManagedEvent: WithTopic + Clone {
	type Manager: ManageTopics<Event = Self>;
}

pub trait ManageTopics {
	type Event: WithTopic + Clone;

	fn new(capacity: usize) -> Self;
	fn get_sender(&self, topic: <Self::Event as WithTopic>::Topic) -> &TokioBroadcastSender<Self::Event>;
	fn create_receiver(&self, topic: <Self::Event as WithTopic>::Topic) -> TokioBroadcastReceiver<Self::Event>;
}
