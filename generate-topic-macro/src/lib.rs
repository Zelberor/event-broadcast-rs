use proc_macro::TokenStream;
use quote::quote;
use syn::{self, spanned::Spanned};

#[proc_macro_attribute]
pub fn generate_topic(_attr: TokenStream, item: TokenStream) -> TokenStream {
	let item = syn::parse_macro_input!(item);
	let item_enum = match item {
		syn::Item::Enum(e) => e,
		_ => return syn::Error::new(item.span(), "Only enums supported").to_compile_error().into(),
	};

	let item_enum_clone = item_enum.clone();
	let general_span = item_enum.span().clone();

	let event_ident = item_enum.ident;

	let mut topic_name = event_ident.to_string();
	topic_name.push_str("Topic");
	let topic_ident = syn::Ident::new(&topic_name, general_span);
	let mut topic_manager_name = event_ident.to_string();
	topic_manager_name.push_str("TopicManager");
	let topic_manager_ident = syn::Ident::new(&topic_manager_name, general_span);

	let mut topic_idents = Vec::new();
	let mut event_to_topic = Vec::new();
	let mut usize_to_topic = Vec::new();
	let mut topic_to_usize = Vec::new();
	let mut broadcast_channel_idents = Vec::new();

	let enum_variants = item_enum.variants;
	for (i, enum_variant) in enum_variants.iter().enumerate() {
		let variant_ident = &enum_variant.ident;
		topic_idents.push(variant_ident.clone());

		event_to_topic.push(quote! {
			#event_ident::#enum_variant => #topic_ident::#variant_ident
		});

		usize_to_topic.push(quote! {
			#i => Ok(#topic_ident::#variant_ident)
		});

		topic_to_usize.push(quote! {
			#topic_ident::#variant_ident => #i
		});

		broadcast_channel_idents.push(syn::Ident::new(&format!("broadcast_channel_{}", i), general_span));
	}

	let pkg_ident = syn::Ident::new("event_broadcast", general_span);

	let broadcast_channel_num = broadcast_channel_idents.len();

	let gen = quote! {
		#item_enum_clone

		impl #pkg_ident::WithTopic for #event_ident {
			type Topic = #topic_ident;
		}

		impl #pkg_ident::_ManagedEvent for #event_ident {
			type Manager = #topic_manager_ident;
		}

		#[derive(Clone, Copy, Debug)]
		#[repr(usize)]
		pub enum #topic_ident {
			#( #topic_idents ),*
		}

		impl From<#event_ident> for #topic_ident {
			fn from(value: Event) -> Self {
				match value {
					#( #event_to_topic ),*
				}
			}
		}

		impl TryFrom<usize> for #topic_ident {
			type Error = ();

			fn try_from(value: usize) -> Result<Self, Self::Error> {
				match value {
					#( #usize_to_topic, )*
					_ => Err(()),
				}
			}
		}

		impl From<#topic_ident> for usize {
			fn from(topic: #topic_ident) -> Self {
				match topic {
					#( #topic_to_usize ),*
				}
			}
		}

		pub struct #topic_manager_ident {
			event_senders: [#pkg_ident::_TokioBroadcastSender<#event_ident >; #broadcast_channel_num]
		}

		impl #pkg_ident::_ManageTopics for #topic_manager_ident {
			type Event = #event_ident;

			fn new(capacity: usize) -> Self {
				#( let #broadcast_channel_idents = #pkg_ident::_create_broadcast(capacity); )*
				Self {
					event_senders: [#( #broadcast_channel_idents.0 ),*],
				}
			}

			fn get_sender(&self, topic: <Self::Event as #pkg_ident::WithTopic>::Topic) -> &#pkg_ident::_TokioBroadcastSender<Self::Event> {
				let index: usize = topic.into();
				&self.event_senders[index]
			}

			fn create_receiver(&self, topic: <Self::Event as #pkg_ident::WithTopic>::Topic) -> #pkg_ident::_TokioBroadcastReceiver<Self::Event> {
				let index: usize = topic.into();
				self.event_senders[index].subscribe()
			}
		}
	};
	gen.into()
}
